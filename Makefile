SRC=NameList.cpp 
GTEST_SRC=NameListTestHarness.cpp
EXECTUABLE=appName
TEST_EXE=gtests
FLAGS=-std=c++11 -g -m64
LIBS= -L/usr/lib64 -lpthread -L/usr/lib64 -lgtest -lgtest_main
CXX= g++

all : runtime gtest
.PHONEY : all

gtest : 
	g++ $(FLAGS) -o $(TEST_EXE) gtestMain.cpp $(SRC) $(GTEST_SRC) $(LIBS)

runtime : 
	g++ $(FLAGS) -o $(EXECTUABLE) main.cpp $(SRC) $(LIBS)

clean:
	rm Debug/*.o
	rm $(EXECTUABLE)
	rm $(TEST_EXE)
